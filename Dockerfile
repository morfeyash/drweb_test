FROM ubuntu:14.04
MAINTAINER morfeyash

RUN apt-get -yqq update
RUN apt-get -yqq install gcc make librpmbuild3 librpm-dev rpm rpm-common

ADD ./ /opt/epm
WORKDIR /opt/epm

RUN ./configure --bindir=/usr/bin --libdir=/usr/lib/epm --datarootdir=/usr/share/epm
RUN make
RUN /opt/epm/epm --output-dir ./pkg -m '' -v -f rpm epm epm.list
RUN /opt/epm/epm --output-dir ./pkg -m '' -v -f deb epm epm.list
